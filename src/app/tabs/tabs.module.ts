import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { MusicPlayerService } from '../services/music-player.service';
import { MusicControls } from '@ionic-native/music-controls/ngx';
import { Media } from '@ionic-native/media/ngx';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [TabsPage],
  providers: [
    MusicPlayerService,
    MusicControls,
    Media,
  ],
})
export class TabsPageModule {}
