export interface MusicControlsDescription {
    track: string;
    artist: string;
    cover: string;
    hasPrev: boolean;
    hasNext: boolean;
}
