import { Component } from '@angular/core';

import { MusicPlayerService } from '../services/music-player.service';
import { MusicControlsDescription } from '../interfaces/music-controls-description.interface';
import { Platform } from '@ionic/angular';
import { Media, MediaObject } from '@ionic-native/media/ngx';

declare const cordova: any;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    private volume = 1.0;
    private playRef: MediaObject;

    public constructor(
        private player: MusicPlayerService,
        private media: Media,
        private platform: Platform,
    ) {
        const desc: MusicControlsDescription = {
            artist: 'Some artist',
            track: 'Some title',
            cover: null,
            hasNext: false,
            hasPrev: false,
        };
        //this.player.setMediaUri('http://stream01.superfly.fm:8080/live128', desc);
        this.platform.ready().then(() => {
            if (!!cordova) {
                cordova.plugins.backgroundMode.enable();
                console.log('Enabled background mode');
            } else {
                console.log('Did not find CORDOVA - could not enable background mode');
            }
        });
    }

    private disableBackgroundMode(): void {
        if (!!cordova) {
            if (!!cordova) {
                cordova.plugins.backgroundMode.disable();
                console.log('Disabled background mode');
            } else {
                console.log('Did not find CORDOVA - could not disable background mode');
            }
        }
    }

    play(): void {
        //this.player.play();
        if (!this.playRef) {
            this.playRef = this.media.create('http://stream01.superfly.fm:8080/live128');
            this.playRef.onStatusUpdate.subscribe(status => {
                console.log('streaming-test: status changed - ', status);
            });
            this.playRef.onError.subscribe(error => console.log('Error occurred!', error));
        }

        this.playRef.setVolume(this.volume);
        this.playRef.play();
    }

    stop(): void {
        //this.player.stop();

        if (!this.playRef) {
            console.log('No reference found!');
        }

        this.playRef.stop();
        this.playRef.release();
        this.playRef = null;
        this.disableBackgroundMode();
    }

    changeVolume(): void {
        this.volume -= 0.1;

        if (this.volume < 0) {
            this.volume = 1;
        }

        this.playRef.setVolume(this.volume);
    }
}
