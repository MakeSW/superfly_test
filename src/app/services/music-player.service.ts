import { Injectable, OnDestroy } from '@angular/core';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { MusicControls } from '@ionic-native/music-controls/ngx';
import { Subscription } from 'rxjs';
import { MusicControlsDescription } from '../interfaces/music-controls-description.interface';

@Injectable()
export class MusicPlayerService implements OnDestroy {
    private currentMedia: MediaObject;
    private musicControlsSubscription: Subscription;

    public constructor(
        private media: Media,
        private musicControls: MusicControls,
    ) {
    }

    private setupMusicControlsSubscription(): void {
        this.musicControlsSubscription = this.musicControls.subscribe().subscribe(action => {
            const obj = JSON.parse(action) || {};
            const message = obj.message;

            switch (message) {
                case 'music-controls-play':
                    this.play();
                    break;
                case 'music-controls-pause':
                    this.stop();
                    break;
                default:
                    console.log('I am doing nothing!', message);
            }
        });
        this.musicControls.listen();
    }

    private cleanUpMusicControlsSubscription(): void {
        if (!!this.musicControlsSubscription) {
            this.musicControlsSubscription.unsubscribe();
        }
    }

    public setMediaUri(uri: string, description: MusicControlsDescription = null): void {
        this.stop();

        this.musicControls.create({
            track: description.track,
            artist: description.artist,
            cover: description.cover,
            dismissable: false,
            hasPrev: description.hasPrev,
            hasNext: description.hasNext
        });

        this.currentMedia = this.media.create(uri);
        this.currentMedia.onStatusUpdate.subscribe(status => console.log('status', status));
        this.currentMedia.onSuccess.subscribe(status => console.log('success', status));
        this.currentMedia.onError.subscribe(status => console.log('onError', status));
        console.log('created media object', this.currentMedia);
    }

    public play(): void {
        if (!!this.currentMedia) {
            this.currentMedia.play();
            this.musicControls.updateIsPlaying(true);
            console.log('started track!');
        }
    }

    public stop(): void {
        if (!!this.currentMedia) {
            this.currentMedia.stop();
            this.musicControls.updateIsPlaying(false);
        }
    }

    ngOnDestroy(): void {
        if (!!this.musicControlsSubscription) {
            this.musicControlsSubscription.unsubscribe();
        }

        if (!!this.currentMedia) {
            this.currentMedia.release();
        }
    }
}
